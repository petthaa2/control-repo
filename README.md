This control-repo is only used to automatically deploy our 2 redundant mailservers to our openstack environment. 

The module that does all the important stuff is at:
https://bitbucket.org/Larsern1991/mailserver

This module only contains a Puppetfile to ensure all the needed modules are present and a preconfigured site.pp for ease of use for our own infrastructure.
